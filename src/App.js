import './App.css';
import Navbars from "./Components/Navbars";
import { Header } from "./Components/Header";
import { Footer } from "./Components/Footer";
import Cards from "./Components/Cards";
import Grid from '@mui/material/Grid';

function App() {
  return (
    <div className="App">
      <Navbars />
      <Header />

      <Grid container spacing={4} sx={{
        display:'flex', justifyContent:'center', paddingTop:'50px'
      }}>
 
        <Cards/>
  
      </Grid>
      <Footer />
    </div>
  );
}

export default App;
