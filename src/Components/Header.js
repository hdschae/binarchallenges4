import React, { useState, useEffect } from "react";
import Grid from "@mui/material/Grid";
import CarImage from "../img/img_car.png";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import InputAdornment from "@mui/material/InputAdornment";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Button from "@mui/material/Button";
import Select from "@mui/material/Select";
import TextField from "@mui/material/TextField";
import PeopleAltOutlinedIcon from "@mui/icons-material/PeopleAltOutlined";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DatePicker from "@mui/lab/DatePicker";
import TimePicker from "@mui/lab/TimePicker";

export const Header = () => {
  const [driver, setDriver] = useState("");
  const [passanger, setPassanger] = useState(0);
  const [value, setValue] = React.useState(null);
  const [data, setData] = useState([]);

  useEffect(() => {
    var axios = require("axios");

    var config = {
      method: "get",
      url: "https://rent-cars-api.herokuapp.com/admin/car",
      headers: {},
    };

    axios(config)
      .then(function (response) {
        setData(response.data);
        console.log("API was called");
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  const handleChange = (event) => {
    setDriver(event.target.value);
  };

  return (
    <div>
      <Grid container spacing={0} sx={{ backgroundColor: "#F1F3FF" }}>
        <Grid item xs={6} sx={{ my: "auto", px: 3 }}>
          <Container>
            <Typography variant="h4">
              Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)
            </Typography>
            <Typography width={"75%"}>
              Selamat datang di Binar Car Rental. Kami menyediakan mobil
              kualitas terbaik dengan harga terjangkau. Selalu siap melayani
              kebutuhanmu untuk sewa mobil selama 24 jam.
            </Typography>
          </Container>
        </Grid>
        <Grid item xs={6} sx={{ overflow: "hidden" }}>
          <img src={CarImage} alt="" />
        </Grid>
      </Grid>
      <Box
        sx={{
          width: "90%",
          backgroundColor: "#ffff",
          borderRadius: "8px",
          boxShadow: 3,
          p: 2,
          height: "100px",
          top: "-75px",
          position: "relative",
          mx: "auto",
        }}
      >
        <Grid container spacing={2} columns={14}>
          <Grid item xs={3}>
            <Typography>Nama Mobil</Typography>
            <FormControl fullWidth>
              <InputLabel id="demo-simple-select-label" color="success">
                Pilih Nama Mobil
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={driver}
                label="Pilih Tipe Driver"
                color="success"
                onChange={handleChange}
              >
                <MenuItem value={"APV"} color="success">
                  APV
                </MenuItem>
                <MenuItem value={"Tanpa Sopir"} color="success">
                  Tanpa Sopir (Lepas Kunci)
                </MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={3}>
            <Typography>Tanggal</Typography>
            <FormControl fullWidth>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DatePicker
                  label="Pilih Tanggal"
                  value={value}
                  onChange={(newValue) => {
                    setValue(newValue);
                  }}
                  renderInput={(params) => <TextField {...params} />}
                />
              </LocalizationProvider>
            </FormControl>
          </Grid>
          <Grid item xs={3}>
            <Typography>Waktu Jemput / Ambil</Typography>
            <FormControl fullWidth>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <TimePicker
                  label="Pilih Waktu"
                  value={value}
                  onChange={(newValue) => {
                    setValue(newValue);
                  }}
                  renderInput={(params) => <TextField {...params} />}
                />
              </LocalizationProvider>
            </FormControl>
          </Grid>
          <Grid item xs={3}>
            <Typography>Jumlah Penumpang (optional)</Typography>
            <FormControl fullWidth>
              <TextField
                value={passanger}
                id="outlined-number"
                color="success"
                type="number"
                InputLabelProps={{
                  shrink: false,
                }}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <PeopleAltOutlinedIcon />
                    </InputAdornment>
                  ),
                }}
                onChange={(event) => {
                  setPassanger(event.target.value);
                }}
              />
            </FormControl>
          </Grid>
          <Grid item xs={2} sx={{ alignSelf: "center" }}>
            <Button variant="contained" color="success" sx={{ py: 2, mt: 2 }}>
              Cari Mobil
            </Button>
          </Grid>
        </Grid>
      </Box>
    </div>
  );
};
