import React, { useEffect, useState } from 'react'
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from "@mui/material/CardMedia";
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import PeopleAltOutlinedIcon from "@mui/icons-material/PeopleAltOutlined";
import SettingsIcon from "@mui/icons-material/Settings";
import CalendarTodayIcon from "@mui/icons-material/CalendarToday";
import { ListItem, ListItemIcon, ListItemText } from "@mui/material";
import List from "@mui/material/List";
import Grid from '@mui/material/Grid';
import axios from 'axios';
  
function Cards() {
  const [dataCar,setDataCar] = useState([])
  
  useEffect(()=>{
    axios.get('https://rent-cars-api.herokuapp.com/admin/car').then((res) => {
      console.log(res.data)
      setDataCar(res.data)
  })
},[])

  return (
    <>
    {dataCar.map((item)=>{
       return <div className='Cards'>
         
        <Box sx={{ minWidth: 275 }}>
        <Grid item xs={4}>
        <Card variant="outlined" sx={{ width: 333, height: 600, ml: 6, px: 3, py: 1, boxShadow: 2, mt: 5 }}>
        <CardMedia
        component="img"
        height="auto"
        width="250"
        image={item.image}
        alt="green iguana"
        />
        <CardContent>
        <Typography gutterBottom variant="subtitle1" component="div">
          {item.name}
        </Typography>
        <Typography
          variant="h5"
          color="text.secondary"
          sx={{ fontWeight: "bold" }}
        >
          Rp. {item.price}
        </Typography>
        <Typography variant="subtitle2" sx={{ mt: 2 }}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua.
        </Typography>
        <List>
          <ListItem>
            <ListItemIcon>
              <PeopleAltOutlinedIcon />
            </ListItemIcon>
            <ListItemText primary="4 orang" />
          </ListItem>
          <ListItem>
            <ListItemIcon>
              <SettingsIcon />
            </ListItemIcon>
            <ListItemText primary="Manual" />
          </ListItem>
          <ListItem>
            <ListItemIcon>
              <CalendarTodayIcon />
            </ListItemIcon>
            <ListItemText primary="Tahun 2020" />
          </ListItem>
        </List>
      </CardContent>
      <CardActions>
        <Button
          variant="contained"
          color="success"
          sx={{ mb: 4, ml: 1, width: 300 }}
        >
          Pilih Mobil
        </Button>
      </CardActions>    
     </Card>
     </Grid>
   </Box>
   </div>
    })}
    </>
    

   
  )


}


export default Cards